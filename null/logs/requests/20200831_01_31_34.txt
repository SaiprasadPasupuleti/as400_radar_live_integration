<Quote>
		<QuotationDate value="9/1/2020  "/>
		<InceptionDate value="10/1/2020  "/>
		<RatingVariables>
			<SumInsured>
				<Building value="1000000"/>
				<PlantMachinery value="1000000"/>
				<FurnitureFixtureFittings value="0"/>
				<StocksInTrade value="0"/>
				<Others01 value="0"/>
				<Others02 value="0"/>			
			</SumInsured>
			<TradeCode value="3604"/>		
			<ConstructionClass value="1A"/>
			<Warranty value="24A"/>
			<Clause value=""/>
		</RatingVariables>
		<FEA>
			<InternalFEA>		
				<PortableFireExtinguisher value="Y"/>
				<HoseReels value="Y"/>
				<InternalHydrants value="Independent"/>
				<DryRiser value="N"/>
				<WetRiser value="N"/>
				<AutoAlarm value="N"/>
				<GasExtinguishing value="1000"/>
			</InternalFEA>	
				<MobilePowerPumps value="N"/>
				<ExternalHydrants value="Public"/>	
				<ExternalDrenchers value="N"/>
				<AutoSprinklers value="0.4"/>
				<PrivateFireTeam value="Y"/>		
		</FEA>
		<BuildingAgeSecurity>
			<BuildingAge value="15"/>
			<SecurityGuards value="Y"/>		
		</BuildingAgeSecurity>
		<SpecialPerils>	
			<AircraftDamage value="N"/>
			<EarthquakeVolcano value="N"/>
			<StormTempest value="N"/>
			<Flood value="N"/>
			<ImpactDamage value="N"/>
			<BushLalangFire value="N"/>
			<SubsidenceLandslip value="N"/>
			<DamageByFallingTrees value="N"/>
			<Explosion value="NA"/>
			<BWP value="NA"/>
			<RSMD value="NA"/>	
		</SpecialPerils>
	</Quote>