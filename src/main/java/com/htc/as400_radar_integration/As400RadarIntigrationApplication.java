package com.htc.as400_radar_integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class As400RadarIntigrationApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(As400RadarIntigrationApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(As400RadarIntigrationApplication.class);
	}
}
