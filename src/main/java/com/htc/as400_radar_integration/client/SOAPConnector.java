package com.htc.as400_radar_integration.client;

import com.htc.as400_radar_integration.common.Constant;
import com.htc.as400_radar_integration.util.WsHttpHeaderCallback;
import com.towerswatson.rto.dpo.services._2010._01.PofRequest;
import com.towerswatson.rto.dpo.services._2010._01.PofRequestUsingKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class SOAPConnector extends WebServiceGatewaySupport {

    @Value("${soapAction.getPofSoapAction}")
    private String getPofSoapAction;

    public Object callWebService(String url, PofRequest pofRequest){
        return getWebServiceTemplate().marshalSendAndReceive(url,pofRequest, new WsHttpHeaderCallback(Constant.SUBSCRIPTION_KEY, Constant.SUBSCRIPTION_VALUE, getPofSoapAction));
    }
    public Object callWebService(String url, PofRequestUsingKey requestUsingKey, WebServiceMessageCallback webServiceMessageCallback){
        return getWebServiceTemplate().marshalSendAndReceive(url,requestUsingKey, webServiceMessageCallback);
    }
}
