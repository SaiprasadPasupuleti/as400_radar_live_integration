package com.htc.as400_radar_integration.common;

public class Constant {
    public static final String POF_WITH_KEY = "getPofWithKey";

    private Constant() {
    }
    public static final String ROOT_PATH = "pof";
    public static final String POF_ENDPOINT = "/getPofData";
    public static final String POF_COLLECTION = "PofrCollection";
    public static final String KEY_NAME = "KeyName";
    public static final String KEY_REQUEST_TIME = "KeyRequestTime";
    public static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    public static final String SUBSCRIPTION_VALUE = "B7663828ad7a486fa55d76ff00f5dd72";
}
