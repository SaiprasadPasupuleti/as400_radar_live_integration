package com.htc.as400_radar_integration.config;

import com.htc.as400_radar_integration.client.SOAPConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

@Configuration
public class Config {

    @Value("${radar.endpointURL}")
    private String endPointURL;

    @Bean
    public Jaxb2Marshaller marshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        String packageToScan[] = {"com.towerswatson.rto.dpo"};
        marshaller.setPackagesToScan(packageToScan);
        return marshaller;
    }
    @Bean
    public SOAPConnector soapConnector(Jaxb2Marshaller jaxb2Marshaller){
        SOAPConnector client = new SOAPConnector();
        client.setDefaultUri(endPointURL);
        client.setMarshaller(jaxb2Marshaller);
        client.setUnmarshaller(jaxb2Marshaller);
        client.setMessageFactory(messageFactory());
        return client;
    }
    @Bean
    public SaajSoapMessageFactory messageFactory() {
        SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory();
        messageFactory.setSoapVersion(SoapVersion.SOAP_12);
        return messageFactory;
    }
}
