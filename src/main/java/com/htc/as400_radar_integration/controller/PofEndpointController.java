package com.htc.as400_radar_integration.controller;

import com.htc.as400_radar_integration.common.Constant;
import com.htc.as400_radar_integration.service.PofService;
import com.htc.as400_radar_integration.service.SOAPConnectorService;
import com.htc.as400_radar_integration.util.JaxbUtil;
import com.towerswatson.rto.dpo.services._2010._01.PofRequest;
import com.towerswatson.rto.dpo.services._2010._01.PofRequestUsingKey;
import com.towerswatson.rto.dpo.services._2010._01.PofResponse;
import com.towerswatson.rto.dpo.services._2010._01.PofResponse2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

@RestController
@RequestMapping(Constant.ROOT_PATH)
public class PofEndpointController {

    @Autowired
    JaxbUtil jaxbUtil;
    @Autowired
    SOAPConnectorService soapConnectorService;

    @PostMapping(Constant.POF_ENDPOINT)
    public String generateCDATA(@RequestBody String xmlString) throws JAXBException {
        return soapConnectorService.getPofData(new PofRequest(), xmlString);
    }

    @PostMapping(Constant.POF_WITH_KEY)
    public String getPofWithKey(@RequestBody String xmlString, @RequestParam("pofKeyName") String pofKeyName, @RequestParam("pofKetRequestTime") String pofKetRequestTime) throws JAXBException, ParseException, IOException {
        System.out.println("Request coming from As400 .." + xmlString);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println(System.getenv("CATALINA_HOME"));
        Path requestsPath = Paths.get((System.getenv("CATALINA_HOME")) + "/logs/requests/");
        Path responsePath = Paths.get((System.getenv("CATALINA_HOME")) + "/logs/responses/");
        DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("yyyyMMdd_hh_mm_ss");

        ZonedDateTime zdt = ZonedDateTime.now();

        String zdtString = FOMATTER.format(zdt);
        Files.createDirectories(requestsPath);
        Files.createDirectories(responsePath);
        String requestFilename = requestsPath.toString() + "/" + zdtString + ".txt";
        String responseFilename = responsePath.toString() + "/" + zdtString + ".txt";
        if (!StringUtils.isEmpty(xmlString)) {
            if (!Files.exists(Paths.get(requestFilename))) {
                Files.write(Paths.get(requestFilename), (xmlString.replaceAll("\\s+","").getBytes()));
            }

        }
        String responseFromRadar = soapConnectorService.getPofDataWithKey(new PofRequestUsingKey(), xmlString, pofKeyName, pofKetRequestTime);
        stopWatch.stop();
        System.out.println("Totoal time taken to process the As400 request...."+stopWatch.getTotalTimeMillis()+"ms");
        if (!StringUtils.isEmpty(responseFromRadar)) {
            Files.write(Paths.get(responseFilename), (responseFromRadar.getBytes()));
        }
        System.out.println("Response to As400 from REG...." + responseFromRadar);
        return responseFromRadar;
    }

    @GetMapping("/getQuote")
    public String getData() throws JAXBException {
        return "<![CDATA[<root><Quote><YearOfManufacture value=\"2001\" /><NumberOfDoors value=\"3\" /><Value value=\"10000\" /><NumberOfSeats value=\"4\" /></Quote></root>]]>";
    }
}
