package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlAccessorType(XmlAccessType.PROPERTY)
public class BuildingAgeSecurity implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "BuildingAge", required = true)
     private byte BuildingAge;
    @XmlElement(name = "SecurityGuards", required = true)
     private String SecurityGuards;

    public BuildingAgeSecurity() {
    }

    public BuildingAgeSecurity(byte buildingAge, String securityGuards) {
        BuildingAge = buildingAge;
        SecurityGuards = securityGuards;
    }

    public byte getBuildingAge() {
        return BuildingAge;
    }

    public void setBuildingAge(byte buildingAge) {
        BuildingAge = buildingAge;
    }

    public String getSecurityGuards() {
        return SecurityGuards;
    }

    public void setSecurityGuards(String securityGuards) {
        SecurityGuards = securityGuards;
    }
}
