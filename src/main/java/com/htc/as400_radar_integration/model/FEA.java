package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
@XmlAccessorType(XmlAccessType.PROPERTY)
public class FEA implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "InternalFEA", required = true)
    InternalFEA InternalFEA;
    @XmlElement(name = "MobilePowerPumps", required = true)
    private String MobilePowerPumps;
    @XmlElement(name = "ExternalHydrants", required = true)
    private String ExternalHydrants;
    @XmlElement(name = "ExternalDrenchers", required = true)
    private String ExternalDrenchers;
    @XmlElement(name = "AutoSprinklers", required = true)
    private BigDecimal AutoSprinklers;
    @XmlElement(name = "PrivateFireTeam", required = true)
    private String PrivateFireTeam;

    public FEA() {
    }

    public FEA(com.htc.as400_radar_integration.model.InternalFEA internalFEA, String mobilePowerPumps, String externalHydrants, String externalDrenchers, BigDecimal autoSprinklers, String privateFireTeam) {
        InternalFEA = internalFEA;
        MobilePowerPumps = mobilePowerPumps;
        ExternalHydrants = externalHydrants;
        ExternalDrenchers = externalDrenchers;
        AutoSprinklers = autoSprinklers;
        PrivateFireTeam = privateFireTeam;
    }

    public InternalFEA getInternalFEA() {
        return InternalFEA;
    }

    public void setInternalFEA(InternalFEA internalFEA) {
        InternalFEA = internalFEA;
    }

    public String getMobilePowerPumps() {
        return MobilePowerPumps;
    }

    public void setMobilePowerPumps(String mobilePowerPumps) {
        MobilePowerPumps = mobilePowerPumps;
    }

    public String getExternalHydrants() {
        return ExternalHydrants;
    }

    public void setExternalHydrants(String externalHydrants) {
        ExternalHydrants = externalHydrants;
    }

    public String getExternalDrenchers() {
        return ExternalDrenchers;
    }

    public void setExternalDrenchers(String externalDrenchers) {
        ExternalDrenchers = externalDrenchers;
    }

    public BigDecimal getAutoSprinklers() {
        return AutoSprinklers;
    }

    public void setAutoSprinklers(BigDecimal autoSprinklers) {
        AutoSprinklers = autoSprinklers;
    }

    public String getPrivateFireTeam() {
        return PrivateFireTeam;
    }

    public void setPrivateFireTeam(String privateFireTeam) {
        PrivateFireTeam = privateFireTeam;
    }
}
