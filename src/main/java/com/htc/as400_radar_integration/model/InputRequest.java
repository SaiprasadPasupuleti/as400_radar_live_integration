package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Quote")
public class InputRequest {
    @XmlElement(name = "YearOfManufacture", required = true)
    private int YearOfManufacture;
    @XmlElement(name = "NumberOfDoors", required = true)
    private int NumberOfDoors;
    @XmlElement(name = "NumberOfSeats", required = true)
    private int NumberOfSeats;
    @XmlElement(name = "Value", required = true)
    private int Value;

    public int getYearOfManufacture() {
        return YearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        YearOfManufacture = yearOfManufacture;
    }

    public int getNumberOfDoors() {
        return NumberOfDoors;
    }

    public void setNumberOfDoors(int numberOfDoors) {
        NumberOfDoors = numberOfDoors;
    }

    public int getNumberOfSeats() {
        return NumberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        NumberOfSeats = numberOfSeats;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }
}
