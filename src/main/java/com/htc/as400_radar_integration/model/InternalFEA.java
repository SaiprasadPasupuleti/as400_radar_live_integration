package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class InternalFEA implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "PortableFireExtinguisher", required = true)
    private String PortableFireExtinguisher;
    @XmlElement(name = "HoseReels", required = true)
    private String HoseReels;
    @XmlElement(name = "InternalHydrants", required = true)
    private String InternalHydrants;
    @XmlElement(name = "DryRiser", required = true)
    private String DryRiser;
    @XmlElement(name = "WetRiser", required = true)
    private String WetRiser;
    @XmlElement(name = "AutoAlarm", required = true)
    private String AutoAlarm;
    @XmlElement(name = "GasExtinguishing", required = true)
    private short GasExtinguishing;

    public InternalFEA() {
    }

    public InternalFEA(String portableFireExtinguisher, String hoseReels, String internalHydrants, String dryRiser, String wetRiser, String autoAlarm, short gasExtinguishing) {
        PortableFireExtinguisher = portableFireExtinguisher;
        HoseReels = hoseReels;
        InternalHydrants = internalHydrants;
        DryRiser = dryRiser;
        WetRiser = wetRiser;
        AutoAlarm = autoAlarm;
        GasExtinguishing = gasExtinguishing;
    }

    public String getPortableFireExtinguisher() {
        return PortableFireExtinguisher;
    }

    public void setPortableFireExtinguisher(String portableFireExtinguisher) {
        PortableFireExtinguisher = portableFireExtinguisher;
    }

    public String getHoseReels() {
        return HoseReels;
    }

    public void setHoseReels(String hoseReels) {
        HoseReels = hoseReels;
    }

    public String getInternalHydrants() {
        return InternalHydrants;
    }

    public void setInternalHydrants(String internalHydrants) {
        InternalHydrants = internalHydrants;
    }

    public String getDryRiser() {
        return DryRiser;
    }

    public void setDryRiser(String dryRiser) {
        DryRiser = dryRiser;
    }

    public String getWetRiser() {
        return WetRiser;
    }

    public void setWetRiser(String wetRiser) {
        WetRiser = wetRiser;
    }

    public String getAutoAlarm() {
        return AutoAlarm;
    }

    public void setAutoAlarm(String autoAlarm) {
        AutoAlarm = autoAlarm;
    }

    public short getGasExtinguishing() {
        return GasExtinguishing;
    }

    public void setGasExtinguishing(short gasExtinguishing) {
        GasExtinguishing = gasExtinguishing;
    }
}
