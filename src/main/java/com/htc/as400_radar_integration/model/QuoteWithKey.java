package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Quote")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class QuoteWithKey implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "RatingVariables", required = true)
    RatingVariables RatingVariables;
    @XmlElement(name = "FEA", required = true)
    FEA Fea;
    @XmlElement(name = "BuildingAgeSecurity", required = true)
    BuildingAgeSecurity BuildingAgeSecurity;
    @XmlElement(name = "SpecialPerils", required = true)
    SpecialPerils SpecialPerils;

    public QuoteWithKey() {
    }

    public QuoteWithKey(RatingVariables ratingVariables, FEA fea, com.htc.as400_radar_integration.model.BuildingAgeSecurity buildingAgeSecurity, com.htc.as400_radar_integration.model.SpecialPerils specialPerils) {
        RatingVariables = ratingVariables;
        Fea = fea;
        BuildingAgeSecurity = buildingAgeSecurity;
        SpecialPerils = specialPerils;
    }

    public RatingVariables getRatingVariables() {
        return RatingVariables;
    }

    public void setRatingVariables(RatingVariables ratingVariables) {
        RatingVariables = ratingVariables;
    }

    public FEA getFea() {
        return Fea;
    }

    public void setFea(FEA fea) {
        Fea = fea;
    }

    public com.htc.as400_radar_integration.model.BuildingAgeSecurity getBuildingAgeSecurity() {
        return BuildingAgeSecurity;
    }

    public void setBuildingAgeSecurity(com.htc.as400_radar_integration.model.BuildingAgeSecurity buildingAgeSecurity) {
        BuildingAgeSecurity = buildingAgeSecurity;
    }

    public com.htc.as400_radar_integration.model.SpecialPerils getSpecialPerils() {
        return SpecialPerils;
    }

    public void setSpecialPerils(com.htc.as400_radar_integration.model.SpecialPerils specialPerils) {
        SpecialPerils = specialPerils;
    }
}
