package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlAccessorType(XmlAccessType.PROPERTY)
public class RatingVariables implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "SumInsured", required = true)
    SumInsured SumInsured;
    @XmlElement(name = "TradeCode", required = true)
    private short TradeCode;
    @XmlElement(name = "ConstructionClass", required = true)
    private String ConstructionClass;
    @XmlElement(name = "Warranty", required = true)
    private String Warranty;
    @XmlElement(name = "Clause", required = true)
    private String Clause;

    public RatingVariables() {
    }

    public RatingVariables(com.htc.as400_radar_integration.model.SumInsured sumInsured, short tradeCode, String constructionClass, String warranty, String clause) {
        SumInsured = sumInsured;
        TradeCode = tradeCode;
        ConstructionClass = constructionClass;
        Warranty = warranty;
        Clause = clause;
    }

    public SumInsured getSumInsured() {
        return SumInsured;
    }

    public void setSumInsured(SumInsured sumInsured) {
        SumInsured = sumInsured;
    }

    public short getTradeCode() {
        return TradeCode;
    }

    public void setTradeCode(short tradeCode) {
        TradeCode = tradeCode;
    }

    public String getConstructionClass() {
        return ConstructionClass;
    }

    public void setConstructionClass(String constructionClass) {
        ConstructionClass = constructionClass;
    }

    public String getWarranty() {
        return Warranty;
    }

    public void setWarranty(String warranty) {
        Warranty = warranty;
    }

    public String getClause() {
        return Clause;
    }

    public void setClause(String clause) {
        Clause = clause;
    }
}
