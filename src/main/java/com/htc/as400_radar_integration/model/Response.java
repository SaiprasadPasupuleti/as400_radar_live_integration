package com.htc.as400_radar_integration.model;

public class Response {
    private int errorCode;
    private String errorMessage;
    private String radarResponse;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRadarResponse() {
        return radarResponse;
    }

    public void setRadarResponse(String radarResponse) {
        this.radarResponse = radarResponse;
    }

}
