package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SpecialPerils implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "EarthquakeVolcano", required = true)    private String AircraftDamage;
    private String EarthquakeVolcano;
    @XmlElement(name = "StormTempest", required = true)
    private String StormTempest;
    @XmlElement(name = "Flood", required = true)
    private String Flood;
    @XmlElement(name = "ImpactDamage", required = true)
    private String ImpactDamage;
    @XmlElement(name = "BushLalangFire", required = true)
    private String BushLalangFire;
    @XmlElement(name = "SubsidenceLandslip", required = true)
    private String SubsidenceLandslip;
    @XmlElement(name = "DamageByFallingTrees", required = true)
    private String DamageByFallingTrees;
    @XmlElement(name = "Explosion", required = true)
    private String Explosion;
    @XmlElement(name = "BWP", required = true)
    private String Bwp;
    @XmlElement(name = "RSMD", required = true)
    private String Rsmd;

    public SpecialPerils() {
    }

    public SpecialPerils(String aircraftDamage, String earthquakeVolcano, String stormTempest, String flood, String impactDamage, String bushLalangFire, String subsidenceLandslip, String damageByFallingTrees, String explosion, String bwp, String rsmd) {
        AircraftDamage = aircraftDamage;
        EarthquakeVolcano = earthquakeVolcano;
        StormTempest = stormTempest;
        Flood = flood;
        ImpactDamage = impactDamage;
        BushLalangFire = bushLalangFire;
        SubsidenceLandslip = subsidenceLandslip;
        DamageByFallingTrees = damageByFallingTrees;
        Explosion = explosion;
        Bwp = bwp;
        Rsmd = rsmd;
    }

    public String getAircraftDamage() {
        return AircraftDamage;
    }

    public void setAircraftDamage(String aircraftDamage) {
        AircraftDamage = aircraftDamage;
    }

    public String getEarthquakeVolcano() {
        return EarthquakeVolcano;
    }

    public void setEarthquakeVolcano(String earthquakeVolcano) {
        EarthquakeVolcano = earthquakeVolcano;
    }

    public String getStormTempest() {
        return StormTempest;
    }

    public void setStormTempest(String stormTempest) {
        StormTempest = stormTempest;
    }

    public String getFlood() {
        return Flood;
    }

    public void setFlood(String flood) {
        Flood = flood;
    }

    public String getImpactDamage() {
        return ImpactDamage;
    }

    public void setImpactDamage(String impactDamage) {
        ImpactDamage = impactDamage;
    }

    public String getBushLalangFire() {
        return BushLalangFire;
    }

    public void setBushLalangFire(String bushLalangFire) {
        BushLalangFire = bushLalangFire;
    }

    public String getSubsidenceLandslip() {
        return SubsidenceLandslip;
    }

    public void setSubsidenceLandslip(String subsidenceLandslip) {
        SubsidenceLandslip = subsidenceLandslip;
    }

    public String getDamageByFallingTrees() {
        return DamageByFallingTrees;
    }

    public void setDamageByFallingTrees(String damageByFallingTrees) {
        DamageByFallingTrees = damageByFallingTrees;
    }

    public String getExplosion() {
        return Explosion;
    }

    public void setExplosion(String explosion) {
        Explosion = explosion;
    }

    public String getBwp() {
        return Bwp;
    }

    public void setBwp(String bwp) {
        Bwp = bwp;
    }

    public String getRsmd() {
        return Rsmd;
    }

    public void setRsmd(String rsmd) {
        Rsmd = rsmd;
    }
}
