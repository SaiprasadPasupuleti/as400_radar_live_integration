package com.htc.as400_radar_integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class SumInsured implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Building", required = true)
    private int Building ;
    @XmlElement(name = "PlantMachinery", required = true)
    private int PlantMachinery;
    @XmlElement(name = "FurnitureFixtureFittings", required = true)
    private byte FurnitureFixtureFittings;
    @XmlElement(name = "StocksInTrade", required = true)
    private byte StocksInTrade;
    @XmlElement(name = "Others01", required = true)
    private byte Others01;
    @XmlElement(name = "Others02", required = true)
    private byte Others02;

    public SumInsured() {
    }

    public SumInsured(int building, int plantMachinery, byte furnitureFixtureFittings, byte stocksInTrade, byte others01, byte others02) {
        Building = building;
        PlantMachinery = plantMachinery;
        FurnitureFixtureFittings = furnitureFixtureFittings;
        StocksInTrade = stocksInTrade;
        Others01 = others01;
        Others02 = others02;
    }

    public int getBuilding() {
        return Building;
    }

    public void setBuilding(int building) {
        Building = building;
    }

    public int getPlantMachinery() {
        return PlantMachinery;
    }

    public void setPlantMachinery(int plantMachinery) {
        PlantMachinery = plantMachinery;
    }

    public byte getFurnitureFixtureFittings() {
        return FurnitureFixtureFittings;
    }

    public void setFurnitureFixtureFittings(byte furnitureFixtureFittings) {
        FurnitureFixtureFittings = furnitureFixtureFittings;
    }

    public byte getStocksInTrade() {
        return StocksInTrade;
    }

    public void setStocksInTrade(byte stocksInTrade) {
        StocksInTrade = stocksInTrade;
    }

    public byte getOthers01() {
        return Others01;
    }

    public void setOthers01(byte others01) {
        Others01 = others01;
    }

    public byte getOthers02() {
        return Others02;
    }

    public void setOthers02(byte others02) {
        Others02 = others02;
    }
}
