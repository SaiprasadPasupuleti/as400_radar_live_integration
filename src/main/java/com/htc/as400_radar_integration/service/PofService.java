package com.htc.as400_radar_integration.service;

public interface PofService {
    public String getParsedCDATARequest(String xmlString, String key);
}
