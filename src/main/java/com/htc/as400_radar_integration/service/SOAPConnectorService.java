package com.htc.as400_radar_integration.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.as400_radar_integration.client.SOAPConnector;
import com.htc.as400_radar_integration.common.Constant;
import com.htc.as400_radar_integration.model.Response;
import com.htc.as400_radar_integration.util.WsHttpHeaderCallback;
import com.towerswatson.rto.dpo.services._2010._01.PofRequest;
import com.towerswatson.rto.dpo.services._2010._01.PofRequestUsingKey;
import com.towerswatson.rto.dpo.services._2010._01.PofResponse;
import com.towerswatson.rto.dpo.services._2010._01.PofResponse2;
import com.towerswatson.rto.smf.types._2010._01.PofInformationCollectionDataContract;
import com.towerswatson.rto.smf.types._2010._01.PofInformationDataContract;
import com.towerswatson.rto.smf.types._2010._01.PofrInformationCollectionDataContract;
import com.towerswatson.rto.smf.types._2010._01.PofrInformationDataContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SOAPConnectorService {
    @Autowired
    SOAPConnector soapConnector;
    @Autowired
    private Environment environment;

    @Value("${radar.endpointURL}")
    private String endPointURL;

    @Value("${radar.nameSpaceURI}")
    private String nameSpaceURI;

    @Value("${soapAction.getPofWithKeySoapAction}")
    private String getPofWithKeySoapAction;


    public String getPofData(PofRequest pofRequest, String quoteData) throws JAXBException {
        quoteData = String.format("<root>%s</root>", quoteData);
        PofrInformationCollectionDataContract pofrInformationCollectionDataContract = new PofrInformationCollectionDataContract();
        List<PofrInformationDataContract> pofrInformationDataContractList = new ArrayList<>();
        PofrInformationDataContract pofrInformationDataContract = new PofrInformationDataContract();

        pofrInformationDataContract.setPofr(quoteData);
        pofrInformationDataContractList.add(pofrInformationDataContract);
        pofrInformationCollectionDataContract.getPofrInformationDataContract().add(pofrInformationDataContract);
        JAXBElement<PofrInformationCollectionDataContract> pofrCollection = new JAXBElement(new QName(nameSpaceURI, Constant.POF_COLLECTION), PofrInformationCollectionDataContract.class, pofrInformationCollectionDataContract);
        pofRequest.setPofrCollection(pofrCollection);
        PofResponse responseFromSoap = (PofResponse) soapConnector.callWebService(endPointURL, pofRequest);
        System.out.println("value ---"+responseFromSoap.getPofCollection().getValue().getPofInformationDataContract().get(0).getPof().getValue());
        String premiumValue = responseFromSoap.getPofCollection().getValue().getPofInformationDataContract().get(0).getPof().getValue();
        if (!StringUtils.isEmpty(premiumValue)) {
            return premiumValue.replaceAll("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        } else {
            return null;
        }
    }

    public String getPofDataWithKey(PofRequestUsingKey pofRequestUsingKey, String quoteData, String pofKeyName, String pofKetRequestTime) throws ParseException {
        Response response = new Response();
        quoteData = String.format("<root>%s</root>", quoteData.replaceAll("\\t|\\r\\n", ""));
        PofrInformationCollectionDataContract pofrInformationCollectionDataContract = new PofrInformationCollectionDataContract();
        List<PofrInformationDataContract> pofrInformationDataContractList = new ArrayList<>();
        PofrInformationDataContract pofrInformationDataContract = new PofrInformationDataContract();
        pofrInformationDataContract.setPofr(quoteData);
        pofrInformationDataContractList.add(pofrInformationDataContract);
        pofrInformationCollectionDataContract.getPofrInformationDataContract().add(pofrInformationDataContract);
        JAXBElement<PofrInformationCollectionDataContract> pofrCollection = new JAXBElement(new QName(nameSpaceURI, Constant.POF_COLLECTION), PofrInformationCollectionDataContract.class, pofrInformationCollectionDataContract);
        pofRequestUsingKey.setPofrCollection(pofrCollection);
        pofRequestUsingKey.setKeyName(new JAXBElement(new QName(nameSpaceURI, Constant.KEY_NAME), String.class, pofKeyName));
        pofRequestUsingKey.setKeyRequestTime(new JAXBElement(new QName(nameSpaceURI, Constant.KEY_REQUEST_TIME), XMLGregorianCalendar.class, getXmlCalendar(pofKetRequestTime)));
        PofResponse2 responseFromSoap = (PofResponse2) soapConnector.callWebService(endPointURL, pofRequestUsingKey, new WsHttpHeaderCallback(Constant.SUBSCRIPTION_KEY, Constant.SUBSCRIPTION_VALUE, getPofWithKeySoapAction));
        PofInformationCollectionDataContract pofInformationCollectionDataContract = responseFromSoap.getPofCollection().getValue();
        String premiumValue = null;
        try {

            if (pofInformationCollectionDataContract != null) {
                List<PofInformationDataContract> pofInformationDataContracts = pofInformationCollectionDataContract.getPofInformationDataContract();
                if (pofInformationDataContracts != null && pofInformationDataContracts.size() > 0) {
                    premiumValue = pofInformationDataContracts.get(0).getPof().getValue();
                    if (!StringUtils.isEmpty(premiumValue)) {
                        return createResponse(0,null,premiumValue);
                    } else {
                        return createResponse(1,responseFromSoap.getMetaData().getValue().getErrorMessage(),premiumValue);
                    }
                } else {
                    return createResponse(1,responseFromSoap.getMetaData().getValue().getErrorMessage(),premiumValue);
                }
            }else{
                return createResponse(1,responseFromSoap.getMetaData().getValue().getErrorMessage(),premiumValue);
            }
        }catch (Exception e){
           return createResponse(1,responseFromSoap.getMetaData().getValue().getErrorMessage(),premiumValue);
        }

    }

    private String createResponse(int errorCode, String errorMessage, String radarResponse) {
        Response response = new Response();
        String jsonResponse = null;
        try {
            response.setErrorCode(errorCode);
            response.setErrorMessage(errorMessage);
            response.setRadarResponse(radarResponse.replaceAll("\"|<root>|</root>",""));
            ObjectMapper objectMapper = new ObjectMapper();
           jsonResponse = objectMapper.writeValueAsString(response);

        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }

    public XMLGregorianCalendar getXmlCalendar(String pofKetRequestTime) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = formatter.parse(pofKetRequestTime);
        XMLGregorianCalendar xmlDate = null;
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);

        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            xmlDate.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlDate;
    }
}

