package com.htc.as400_radar_integration.service.impl;

import com.htc.as400_radar_integration.model.InputRequest;
import com.htc.as400_radar_integration.model.QuoteWithKey;
import com.htc.as400_radar_integration.service.PofService;
import com.htc.as400_radar_integration.util.JaxbUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Service
public class PofServiceImpl implements PofService {
    @Autowired
    JaxbUtil jaxbUtil;

    @Override
    public String getParsedCDATARequest(String xmlString, String key) {
        try {
            StringReader stringReader = new StringReader(xmlString);
            JAXBContext jaxbContext = null;
            Unmarshaller unmarshaller = null;
            if (StringUtils.isEmpty(key)) {
                jaxbContext = JAXBContext.newInstance(InputRequest.class);
                unmarshaller = jaxbContext.createUnmarshaller();
                InputRequest inputRequest = (InputRequest) unmarshaller.unmarshal(stringReader);
                return jaxbUtil.jaxbObjectToXML(inputRequest);
            } else {
                StringReader stringReader2 = new StringReader(xmlString.replaceAll("\\r|\\t|\\n", ""));
                jaxbContext = JAXBContext.newInstance(QuoteWithKey.class);
                unmarshaller = jaxbContext.createUnmarshaller();
                QuoteWithKey quoteWithKey = (QuoteWithKey) unmarshaller.unmarshal(stringReader2);
                return jaxbUtil.jaxbObjectToXML(quoteWithKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
