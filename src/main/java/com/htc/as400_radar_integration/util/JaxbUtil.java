package com.htc.as400_radar_integration.util;

import com.htc.as400_radar_integration.model.*;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

@Component
public class JaxbUtil {
    String xml = null;

    public String jaxbObjectToXML(InputRequest request) {
        try {
            Quote quote = new Quote();

            Quote.YearOfManufacture yearOfManufacture = new Quote.YearOfManufacture();
            yearOfManufacture.setValue(request.getYearOfManufacture());
            quote.setYearOfManufacture(yearOfManufacture);

            Quote.NumberOfDoors numberOfDoors = new Quote.NumberOfDoors();
            numberOfDoors.setValue((short) request.getNumberOfDoors());
            quote.setNumberOfDoors(numberOfDoors);

            Quote.NumberOfSeats numberOfSeats = new Quote.NumberOfSeats();
            numberOfSeats.setValue((short) request.getNumberOfSeats());
            quote.setNumberOfSeats(numberOfSeats);

            Quote.Value value = new Quote.Value();
            value.setValue(request.getValue());
            quote.setValue(value);

            JAXBContext jaxbContext = JAXBContext.newInstance(Quote.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(quote, sw);
            System.out.println(sw);
            xml = String.format("<![CDATA[<?xml version=\"1.0\" encoding=\"utf-16\"?><root>%s</root>]]>", sw.toString().trim());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return xml;
    }
    public String jaxbObjectToXML(QuoteWithKey request) throws Exception{
        com.htc.as400_radar_integration.xjc.Quote quote = new com.htc.as400_radar_integration.xjc.Quote();

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables ratingVariables = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables();

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.Clause clause = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.Clause();
        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.Warranty warranty = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.Warranty();
        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.TradeCode tradeCode = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.TradeCode();
        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.ConstructionClass constructionClass = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.ConstructionClass();
        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured sumInsured = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured();

        tradeCode.setValue(request.getRatingVariables().getTradeCode());
        warranty.setValue(request.getRatingVariables().getWarranty());
        clause.setValue(request.getRatingVariables().getClause());
        constructionClass.setValue(request.getRatingVariables().getConstructionClass());

        // setting the Sum insured variables
        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Building building = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Building();
        building.setValue(request.getRatingVariables().getSumInsured().getBuilding());

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.PlantMachinery plantMachinery = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.PlantMachinery();
        plantMachinery.setValue(request.getRatingVariables().getSumInsured().getPlantMachinery());

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.FurnitureFixtureFittings furnitureFixtureFittings = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.FurnitureFixtureFittings();
        furnitureFixtureFittings.setValue(request.getRatingVariables().getSumInsured().getFurnitureFixtureFittings());

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.StocksInTrade stocksInTrade = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.StocksInTrade();
        stocksInTrade.setValue(request.getRatingVariables().getSumInsured().getStocksInTrade());

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Others01 others01 = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Others01();
        others01.setValue(request.getRatingVariables().getSumInsured().getOthers01());

        com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Others02 others02 = new com.htc.as400_radar_integration.xjc.Quote.RatingVariables.SumInsured.Others02();
        others02.setValue(request.getRatingVariables().getSumInsured().getOthers02());

        sumInsured.setBuilding(building);
        sumInsured.setPlantMachinery(plantMachinery);
        sumInsured.setFurnitureFixtureFittings(furnitureFixtureFittings);
        sumInsured.setStocksInTrade(stocksInTrade);
        sumInsured.setOthers01(others01);
        sumInsured.setOthers02(others02);

        // End-of setting sum insured

        ratingVariables.setClause(clause);
        ratingVariables.setWarranty(warranty);
        ratingVariables.setTradeCode(tradeCode);
        ratingVariables.setSumInsured(sumInsured);
        ratingVariables.setConstructionClass(constructionClass);

        //setting up the Fea values
        com.htc.as400_radar_integration.xjc.Quote.FEA fea = new com.htc.as400_radar_integration.xjc.Quote.FEA();

        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA internalFEA = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA();
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.AutoAlarm autoAlarm = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.AutoAlarm();
        autoAlarm.setValue(request.getFea().getInternalFEA().getAutoAlarm());
        internalFEA.setAutoAlarm(autoAlarm);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.DryRiser dryRiser = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.DryRiser();
        dryRiser.setValue(request.getFea().getInternalFEA().getDryRiser());
        internalFEA.setDryRiser(dryRiser);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.GasExtinguishing gasExtinguishing = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.GasExtinguishing();
        gasExtinguishing.setValue(request.getFea().getInternalFEA().getGasExtinguishing());
        internalFEA.setGasExtinguishing(gasExtinguishing);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.HoseReels hoseReels = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.HoseReels();
        hoseReels.setValue(request.getFea().getInternalFEA().getHoseReels());
        internalFEA.setHoseReels(hoseReels);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.InternalHydrants internalHydrants = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.InternalHydrants();
        internalHydrants.setValue(request.getFea().getInternalFEA().getInternalHydrants());
        internalFEA.setInternalHydrants(internalHydrants);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.PortableFireExtinguisher portableFireExtinguisher = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.PortableFireExtinguisher();
        portableFireExtinguisher.setValue(request.getFea().getInternalFEA().getPortableFireExtinguisher());
        internalFEA.setPortableFireExtinguisher(portableFireExtinguisher);
        com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.WetRiser wetRiser = new com.htc.as400_radar_integration.xjc.Quote.FEA.InternalFEA.WetRiser();
        wetRiser.setValue(request.getFea().getInternalFEA().getWetRiser());
        internalFEA.setWetRiser(wetRiser);
        fea.setInternalFEA(internalFEA);
        com.htc.as400_radar_integration.xjc.Quote.FEA.AutoSprinklers autoSprinklers = new com.htc.as400_radar_integration.xjc.Quote.FEA.AutoSprinklers();
        autoSprinklers.setValue(request.getFea().getAutoSprinklers());
        fea.setAutoSprinklers(autoSprinklers);
        com.htc.as400_radar_integration.xjc.Quote.FEA.ExternalDrenchers externalDrenchers = new com.htc.as400_radar_integration.xjc.Quote.FEA.ExternalDrenchers();
        externalDrenchers.setValue(request.getFea().getExternalDrenchers());
        fea.setExternalDrenchers(externalDrenchers);
        com.htc.as400_radar_integration.xjc.Quote.FEA.ExternalHydrants externalHydrants = new com.htc.as400_radar_integration.xjc.Quote.FEA.ExternalHydrants();
        externalHydrants.setValue(request.getFea().getExternalHydrants());
        fea.setExternalHydrants(externalHydrants);
        com.htc.as400_radar_integration.xjc.Quote.FEA.MobilePowerPumps mobilePowerPumps = new com.htc.as400_radar_integration.xjc.Quote.FEA.MobilePowerPumps();
        mobilePowerPumps.setValue(request.getFea().getMobilePowerPumps());
        fea.setMobilePowerPumps(mobilePowerPumps);
        com.htc.as400_radar_integration.xjc.Quote.FEA.PrivateFireTeam privateFireTeam = new com.htc.as400_radar_integration.xjc.Quote.FEA.PrivateFireTeam();
        privateFireTeam.setValue(request.getFea().getPrivateFireTeam());
        fea.setPrivateFireTeam(privateFireTeam);
        //Ending up the Fea setup
        //BuildingAgeSecurity setup
        com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity buildingAgeSecurity = new com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity();
        com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity.BuildingAge buildingAge = new com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity.BuildingAge();
        buildingAge.setValue(request.getBuildingAgeSecurity().getBuildingAge());
        buildingAgeSecurity.setBuildingAge(buildingAge);
        com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity.SecurityGuards securityGuards = new com.htc.as400_radar_integration.xjc.Quote.BuildingAgeSecurity.SecurityGuards();
        securityGuards.setValue(request.getBuildingAgeSecurity().getSecurityGuards());
        buildingAgeSecurity.setSecurityGuards(securityGuards);
        //Ending up BuildingAgeSecurity
        //SpecialPerils setup
        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils specialPerils = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils();

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.AircraftDamage aircraftDamage = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.AircraftDamage();
        aircraftDamage.setValue(request.getSpecialPerils().getAircraftDamage());
        specialPerils.setAircraftDamage(aircraftDamage);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.EarthquakeVolcano earthquakeVolcano = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.EarthquakeVolcano();
        earthquakeVolcano.setValue(request.getSpecialPerils().getEarthquakeVolcano());
        specialPerils.setEarthquakeVolcano(earthquakeVolcano);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.StormTempest stormTempest = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.StormTempest();
        stormTempest.setValue(request.getSpecialPerils().getStormTempest());
        specialPerils.setStormTempest(stormTempest);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.Flood flood = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.Flood();
        flood.setValue(request.getSpecialPerils().getFlood());
        specialPerils.setFlood(flood);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.ImpactDamage impactDamage = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.ImpactDamage();
        impactDamage.setValue(request.getSpecialPerils().getImpactDamage());
        specialPerils.setImpactDamage(impactDamage);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.BushLalangFire bushLalangFire = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.BushLalangFire();
        bushLalangFire.setValue(request.getSpecialPerils().getBushLalangFire());
        specialPerils.setBushLalangFire(bushLalangFire);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.SubsidenceLandslip subsidenceLandslip = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.SubsidenceLandslip();
        subsidenceLandslip.setValue(request.getSpecialPerils().getSubsidenceLandslip());
        specialPerils.setSubsidenceLandslip(subsidenceLandslip);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.DamageByFallingTrees damageByFallingTrees = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.DamageByFallingTrees();
        damageByFallingTrees.setValue(request.getSpecialPerils().getDamageByFallingTrees());
        specialPerils.setDamageByFallingTrees(damageByFallingTrees);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.Explosion explosion = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.Explosion();
        explosion.setValue(request.getSpecialPerils().getExplosion());
        specialPerils.setExplosion(explosion);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.BWP bwp = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.BWP();
        bwp.setValue(request.getSpecialPerils().getBwp());
        specialPerils.setBWP(bwp);

        com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.RSMD rsmd = new com.htc.as400_radar_integration.xjc.Quote.SpecialPerils.RSMD();
        rsmd.setValue(request.getSpecialPerils().getRsmd());
        specialPerils.setRSMD(rsmd);

        quote.setRatingVariables(ratingVariables);
        quote.setFEA(fea);
        quote.setBuildingAgeSecurity(buildingAgeSecurity);
        quote.setSpecialPerils(specialPerils);

        JAXBContext jaxbContext = JAXBContext.newInstance(com.htc.as400_radar_integration.xjc.Quote.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(quote, sw);
        System.out.println(sw);
        xml = String.format("<![CDATA[<root>%s</root>]]>", sw.toString().replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim());
        return xml;
    }
}


