package com.htc.as400_radar_integration.util;

import org.springframework.util.StringUtils;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.transport.HeadersAwareSenderWebServiceConnection;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class WsHttpHeaderCallback implements WebServiceMessageCallback
{
    private String headerKey;
    private String headerValue;
    private String soapAction;

    public WsHttpHeaderCallback(String headerKey, String headerValue, String soapAction)
    {
        super();
        this.headerKey = headerKey;
        this.headerValue = headerValue;
        this.soapAction = soapAction;
    }

    public WsHttpHeaderCallback()
    {
        super();
    }

    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException
    {
        //validateRequiredFields();
        addRequestHeader(headerKey, headerValue);
        if (StringUtils.hasText(this.soapAction)) {
            SaajSoapMessage soapMessage = (SaajSoapMessage) message;
            soapMessage.setSoapAction(this.soapAction);
            SoapHeader soapHeader = soapMessage.getEnvelope().getHeader();
            SoapHeaderElement soapHeaderElement = soapHeader.addHeaderElement(new QName("http://www.w3.org/2005/08/addressing", "Action", "a"));
            soapHeaderElement.setText(soapAction);
            soapHeaderElement.setMustUnderstand(true);
        }
    }

    private void addRequestHeader(String headerKey, String headerValue) throws IOException {
        TransportContext context = TransportContextHolder.getTransportContext();
        HeadersAwareSenderWebServiceConnection connection = (HeadersAwareSenderWebServiceConnection) context.getConnection();
        connection.addRequestHeader(headerKey, headerValue);
    }
}